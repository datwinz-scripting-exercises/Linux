# Linux

## Description
Oefenscripts van Linux. assignments zijn van [Unixerius](https://github.com/unixerius/XK0-004/blob/main/Lesson%20005%20-%20Searching%20and%20analyzing%20text/005%20-%20Assignments.txt), TLDP_Writing_Scripts van [TLDP](https://tldp.org/LDP/abs/html/writingscripts.html) en U_of_Cambridge_exercises, logischerwijs, van de [University of Cambridge](http://www-h.eng.cam.ac.uk/help/tpl/unix/scripts/node16.html).

## License
>"THE BEER-WARE LICENSE" (Revision 42):
><floor.drewes@itvitaelearning.nl> wrote this file. As long as you retain this notice you
>can do whatever you want with this stuff. If we meet some day, and you think
>this stuff is worth it, you can buy me a beer in return Floor Drewes

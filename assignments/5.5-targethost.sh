#!/bin/bash
# Takes a list of target hostnames (or IPs)
if [[ -e hostnames.txt ]]; then
	mkdir ~/setuid
# SSHs to each of these hosts
	cat hostnames.txt | while read host; do
# And on that host, looks for setuid files.
# Output each list to ~/setuid/targethost.txt on the source host!
		ssh $host find / -perm -u=s > ~/setuid/target${host}.txt 2>/dev/null 
	done 
else echo "please make a file named hostnames.txt with seperate hostnames or IPs on each line."
fi


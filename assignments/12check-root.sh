#!/bin/bash
# Check if the root user is active.
# e.g. "ps -fC bash | grep ^root"
# Yes, you will need to write a test with IF. :)
# If root IS logged in, send a warning to journald.
# Use cron to run this script every minute.
# * * * * * /path/to/12check-root.sh
if [[ $(ps -fC bash | grep ^root) ]]; then
  echo root is logged in! | systemd-cat -p warning 
fi


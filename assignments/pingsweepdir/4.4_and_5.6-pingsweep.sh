#!/bin/bash
echo -e "This script pings all IP adresses in your network with netmask /24 five times. It sorts the output and removes the unsorted output.\nIt will also test the ports specified in the \"ports\" file to check if they are "up".\nThe script waits 20 seconds for the pings to finish, this can take longer. Uncomment line 47, 48 and 57 in the script if you don't trust it has finished.\nIf you run this script for the first time, you should do it in a new folder." 
# Find base adress to scan and test if it has netmask /24.
input1=$1
if [[ $input1 =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
	ip a | grep "inet ${input1}/24" &> /dev/null
	if [[ $? -eq 0 ]]; then
		base_ip=$(echo $input1 | cut -d . -f 1,2,3)
	else
		echo "Your IP adress doesn't have the right netmask."
		exit 1
	fi
else
	read -p "What is you IP adress? " user_ip
	if [[ $user_ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
		ip a | grep "inet ${user_ip}/24" &> /dev/null
		if [[ $? -eq 0 ]]; then
			base_ip=$(echo $user_ip | cut -d . -f 1,2,3)
		else
			echo "Your IP adress doesn't have the right netmask."
			exit 1
		fi
	else 
		echo "Please enter a valid IP adress."
		exit 1
	fi
fi
# Remove "sweep" if it exists so output doesn't get cluttered.
if [ -e ./sweep ]; then
	rm ./sweep
fi
# Ping all adresses with netmask /24 five times.
last_octet=1
while [ $last_octet -le 254 ]
do
	ping -c 5 "$base_ip.$last_octet" >> ./sweep 2>>/dev/null &
	(( last_octet++ ))
done
# Wait 20 seconds for the pings to finish.
echo "Waiting 20 seconds."
sleep 10
echo "10 seconds left."
sleep 10
# Create a sorted output file for the succesfull pings and optionally delete the unsorted output file.
# Test ports on the "up" hosts
# Uncomment so the script waits till the pings have finished, including the last fi
#read -p "Do you want to continue [y/n] " weshallcontinue
#if [[ $weshallcontinue == "y" ]]; then
ports=$(cat ./ports)
cat ./sweep | grep "bytes from" | sort -n >> ./sweepsorted
cut -d " " -f 4 ./sweepsorted | sort -u | sed 's/\:/\//g' > ./hoststotest
cat hoststotest | while read line; do
	for port in $ports; do
		timeout 3 bash -c "echo 1 >/dev/tcp/$line$port"	2>/dev/null && echo "$line$port is open" >> ./sweepsorted 
	done
done
#fi
rm ./hoststotest ./sweep


#!/bin/bash
activeuser=$(whoami)
if ! [[ $activeuser == "root" ]]; then
	echo "This script should be run as root."
	exit 1
else 
	read -p "Please give me a number < 20. " numbers
	if [[ $1 == "create" ]]; then
		if ! [[ $numbers -gt 0 && $numbers -lt 20 ]]; then
			echo "Please give me a number between 1 and 20."
			exit 1
		else
			for (( number = 1; number <= $numbers; number++ )); do
				cat /etc/passwd | grep user${number}: &> /dev/null
				if [[ $? -eq 0 ]]; then
					echo "user${number} already exists or there is another problem."
				else
					useradd -m -s /bin/bash "user${number}"
					cat <<- thisistheend > /home/user${number}/welcome.txt
					Welcome to your home directory!
					thisistheend
				fi
			done
		fi
	elif [[ $1 == "remove" ]]; then
		if ! [[ $numbers -gt 0 && $numbers -lt 20 ]]; then
			echo "Please give me a number between 1 and 20."
			exit 1
		else
			for (( number = 1; number <= $numbers; number++ )); do
				cat /etc/passwd | grep user${number}: &> /dev/null
				if [[ $? -eq 0 ]]; then
					userdel -r "user${number}"
				else
					echo "user${number} doesn't exit or there is another problem."
				fi
			done
		fi					
	else 
		echo "You should pass create or remove as your first your first argument."
		exit 1
	fi
fi

